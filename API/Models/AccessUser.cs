﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class AccessUser
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string FullName { get; set; } = null!;

        [StringLength(100)]
        public string Password { get; set; } = null!;

        [StringLength(100)]
        public string Role { get; set; } = null!;

        [StringLength(200)]
        public string UserMessage { get; set; } = null!;

        [StringLength(300)]
        public string AccessToken { get; set; } = null!;
    }
}
