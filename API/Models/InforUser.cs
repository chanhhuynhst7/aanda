﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class InforUser
    {
        public int Id { get; set; }

        [StringLength(100)]
        public string FullName { get; set; } = string.Empty;

        [StringLength(100)]
        public string Password { get; set; } = string.Empty;

        [StringLength(100)] 
        public string Role { get; set; } = string.Empty;

    }
}
