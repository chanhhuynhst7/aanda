﻿using API.Data;
using API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.Metrics;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InforUserController : ControllerBase
    {
        private readonly DataContext _context;

        public InforUserController(DataContext context)
        {
            _context = context;
        }

        // GET: api/InforUser
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InforUser>>> GetInforUser()
        {
            return await _context.InforUsers.ToListAsync();
        }

        // GET: api/Country/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InforUser>> GetInforUser(int id)
        {
            var inforuser = await _context.InforUsers.FindAsync(id);

            if (inforuser == null)
            {
                return NotFound();
            }

            return inforuser;
        }

        // PUT: api/InforUser/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInforUser(int id, InforUser inforuser)
        {
            if (id != inforuser.Id)
            {
                return BadRequest();
            }

            _context.Entry(inforuser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InforUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InforUser
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<InforUser>> PostInforUser(InforUser inforuser)
        {
            _context.InforUsers.Add(inforuser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInforUser", new { id = inforuser.Id }, inforuser);
        }

        // DELETE: api/InforUser/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInforUser(int id)
        {
            var inforuser = await _context.InforUsers.FindAsync(id);
            if (inforuser == null)
            {
                return NotFound();
            }

            _context.InforUsers.Remove(inforuser);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool InforUserExists(int id)
        {
            return _context.InforUsers.Any(e => e.Id == id);
        }

    }
}
